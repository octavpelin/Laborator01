package ro.pub.cs.systems.eim.lab03.phonedialer.general;

import ro.pub.cs.systems.eim.lab03.phonedialer.R;

public interface Constants {

    public static int[] textButtonIds = {
            R.id.D0button,
            R.id.D1button,
            R.id.D2button,
            R.id.D3button,
            R.id.D4button,
            R.id.D5button,
            R.id.D6button,
            R.id.D7button,
            R.id.D8button,
            R.id.D9button,
            R.id.DStbutton,
            R.id.DHashbutton
    };

    public static int[] imageButtonIds = {
            R.id.callButton,
            R.id.terminateButton,
            R.id.eraseButton
    };
    final public static int PERMISSION_REQUEST_CALL_PHONE = 1;
    final public static int CONTACTS_MANAGER_REQUEST_CODE = 2017;
}
