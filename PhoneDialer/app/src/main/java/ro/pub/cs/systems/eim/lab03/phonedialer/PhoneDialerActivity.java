package ro.pub.cs.systems.eim.lab03.phonedialer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.view.View;

import java.util.ArrayList;

import ro.pub.cs.systems.eim.lab03.phonedialer.general.Constants;

public class PhoneDialerActivity extends AppCompatActivity {

    class DialButtonListener implements View.OnClickListener {
        private Button caller;
        private EditText textbox;

        public DialButtonListener(Button caller) {
            this.caller = caller;
        }

        public DialButtonListener withEditText(EditText editText) {
            this.textbox = editText;
            return this;
        }

        @Override
        public void onClick(View v) {
            String editTextStr = this.textbox.getText().toString();
            String buttonChar = this.caller.getText().toString();
            editTextStr += buttonChar;
            this.textbox.setText(editTextStr);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_dialer);

        final EditText dialEditText = (EditText)findViewById(R.id.dialEditText);
        ArrayList<? super View> dialButtons = new ArrayList<View>();

        final ImageButton eraseButton = (ImageButton)findViewById(R.id.eraseButton);
        final ImageButton callButton = (ImageButton)findViewById(R.id.callButton);
        final ImageButton terminateButton = (ImageButton)findViewById(R.id.terminateButton);

        for (int index = 0; index < Constants.textButtonIds.length; index++) {
            Button b0 = (Button)findViewById(Constants.textButtonIds[index]);
            b0.setOnClickListener(new DialButtonListener(b0).withEditText(dialEditText));
            dialButtons.add(b0);
        }

        eraseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String editText = dialEditText.getText().toString();
                if(editText.length() > 0)
                    editText = editText.substring(0, editText.length() - 1);
                dialEditText.setText(editText);
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(PhoneDialerActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            PhoneDialerActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            Constants.PERMISSION_REQUEST_CALL_PHONE);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + dialEditText.getText().toString()));
                    startActivity(intent);
                }
            }
        });

        terminateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
